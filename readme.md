## Files

Filename | Description
-- | --
cv.pdf | Harry Austen's CV
degree.png | Harry Austen's university bachelor's degree certificate
ellen.pdf | Ellen Clewes' CV

## Compiling

Dependencies/Prerequisites:

- [make](https://www.gnu.org/software/make/)
- [rubber](https://gitlab.com/latex-rubber/rubber/)
- [texlive](https://tug.org/texlive/)

To regenerate the PDFs, simply run the following from a POSIX shell:

```shell
make
```
