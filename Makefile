.PHONY: all
all: cv.pdf ellen.pdf

%.pdf: %.tex
	rubber --pdf $<
